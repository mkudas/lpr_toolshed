import json
import requests
import os
import pickle
import argparse

'''
This script will download images in order to calculate the vicus rate
Given a site name and the ip of pase server, this will return a directory
images that are labeled with <status> <plate> <visitid>. If the lpr has 
failed, the image will be failed. This data can be scored to get the 
vicus rate.

notes:
# request for visit
# http://192.168.237.203/v2/visits.json?exit_timestamp!=*&dwell>00:02:00

# request for bays
# http://192.168.237.203/v2/bays.json

#site = '192.168.237.203' #mall of rio
#site = '192.168.237.104' # santa monica
#site = '192.168.237.56' # chadstone
#site = '192.168.237.166'  #mall of emirates
#site = '192.168.237.121' # humber river
#site = '192.168.237.108' # ernst and young
#site = '192.168.237.114' # ft. lt
'''

username = 'admin'

def run_command(command):
    r = requests.get(command,auth=(username,password),timeout=100.0)    
    print "command",command
    print "status code", r.status_code
    text = 'none'
    if r.status_code == 200: 
        text = json.loads(r.text)
    return text
    
def get_image(command):
    print command
    r = requests.get(command,auth=(username,password),timeout=5.0)
    print r.status_code
    if r.status_code == 200:        
        filepath = os.path.join(imagefolder,imagename)
        print filepath
        with open(filepath, 'wb') as f:
            for chunk in r.iter_content():
                f.write(chunk)
    else:
        print "failed", imagename
        
if __name__ == "__main__":  
    parser = argparse.ArgumentParser(description=" this script grabs images from open visits to calculate vicus rate")
    parser.add_argument('sitename', metavar='sitename',type=str,help='server ip')
    parser.add_argument('siteip', metavar='siteip',type=str,help='server ip')
    parser.add_argument('password', metavar='serverpassword',type=str,help='server password')

    args = parser.parse_args()

    site = args.siteip
    sitename = args.sitename
    password = args.password
    print 'sitename {0}, siteip {1},password {2}'.format(sitename,site,password)

    imagefolder = "..\\data\\vikus\\{0}".format(sitename)
    if not os.path.exists(imagefolder):
        os.mkdir(imagefolder)
    visitfile = os.path.join(imagefolder,"visits.pkl")
    baysfile = os.path.join(imagefolder,"bays.pkl")
    outputfile = "..\\data\\vikus\\{0}.csv".format(sitename)



    # grab all open visits
    #http://192.168.237.166/v2/visits.json?exit_timestamp!=*&dwell%3E00:02:00&entry_timestamp%3E{now-1.00:00:00
    command = 'http://{0}/v2/visits.json?exit_timestamp!=*&dwell>00:02:00&entry_timestamp>{{now-1.00:00:00}}'.format(site)
    visitlist = run_command(command)
    with open(visitfile,'w') as of:
        pickle.dump(visitlist,of)

    #grab all bays
    command = 'http://{0}/v2/bays.json'.format(site)
    baylist = run_command(command)
    with open(baysfile,'w') as of:
        pickle.dump(baylist,of)

    # find arh cameras
    # this is a weird hack to remove ARH cameras that are mapped as bays
    # at the mall of emeriates
    arh_bay_ids = []
    if sitename == 'moe':
        for bay in baylist:
            if 'sensor' in bay.keys():
                if 'ARH' in bay['sensor']['address']:
                    arh_bay_ids.append(bay['id'])
    print "ARH",arh_bay_ids

    # save all the bayids, and plate results from bays.json
    with open(outputfile,'w') as outfile:
        header = "bayid,outofservice,occupied,visit_id,platefound,platenumber"
        outfile.write(header+'\n')
        for bay in baylist:
            try:
                #print ','.join([str(x) for x in bay.keys()])
                platename = "no plate"
                visitid = "novisit"
                platename = "noplate"
                if "visit" in bay.keys():
                    visitid = 'arhbay'
                    if "ARH" not in bay['sensor']['address']:
                        #print bay['visit']['id']
                        visitid = bay['visit']['id']
                        if "plate" in bay['visit'].keys():
                            platename = bay['visit']['plate']['text']
                            #print platename
                print ",".join([str(x) for x in [bay['id'],bay['is_out_of_service'],bay['is_occupied'],visitid,platename]]).encode('utf-8').strip()
                outfile.write(",".join([str(x) for x in [bay['id'],bay['is_out_of_service'],bay['is_occupied'],visitid,platename]])+'\n')
            except:
                print "except",bay
                pass

    # grab all the images from visits
    failednum = 0
    for visit in visitlist:
        try:
            visitid = visit['id']
            if "plate" in visit.keys():
                platename = visit['plate']['text']
                imagename = 'success_{0}_{1}.jpg'.format(platename,visitid)
            if "plate_failed" in visit.keys():
                platefailed = visit['plate_failed']
                imagename = "failed_{0}.jpg".format(visitid)
            if visit['bay_id'] in arh_bay_ids:
                continue
            if os.path.isfile(imagename):
                pass
            else:
                print "getting visit", visitid        
                command = 'http://{1}/v2/visits/{0}/lpr.jpg'.format(visitid,site)
                get_image(command)
        except:
            print visit