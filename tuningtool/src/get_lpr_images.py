import json
import requests
import os,sys
from datetime import datetime
import time
import argparse
import returncoords as rc

'''
Script takes a directory of LPR images, runs them through the ARH engine, and creates a csv of the results

to run: python get_lpr_images.py localhost <serverpassword>

notes: this is the command for the ARH webservice
#command = 'http://localhost:8028/plates.json?file-path=c:/Users/Administrator/Desktop/lpr.jpg'

'''

imagefolder = "..\\data\\"
if not os.path.exists(imagefolder):
    os.mkdir(imagefolder)

imageexposure = ['0.2','0.4','0.6','0.8','1.0','1.2','1.4','1.6','1.8']

def run_command(command):
    r = requests.get(command,auth=(username,password),timeout=100.0)    
    print 'status', r.status_code
    text = 'none'
    if r.status_code == 200: 
        text = json.loads(r.text)
    return text
    
def get_image(command,imagename):
    print command
    r = requests.get(command,auth=(username,password),timeout=20.0)
    print r.status_code
    if r.status_code == 200:        
        filepath = os.path.join(imagefolder,imagename)
        print filepath
        with open(filepath, 'wb') as f:
            for chunk in r.iter_content():
                f.write(chunk)
    else:
        print "failed", imagename

def return_bay(bay_id):
    for bay in baylist:
        if bay['id'] == bay_id:
            return bay
    raise ValueError('Couldn\'t find bay')

if __name__ == "__main__":	
	parser = argparse.ArgumentParser(description=" this script grabs images from open visits to optimize exposure values")
	parser.add_argument('siteip', metavar='serverip',type=str,help='server ip')
	parser.add_argument('password', metavar='serverpassword',type=str,help='server password')

	args = parser.parse_args()

	site = args.siteip
	print site
	password = args.password
	print password
	
	username = 'admin'
	visitidlist = []
	print 'starting...'
	while 1:
		# grab all open visits
		command = 'http://{0}/v2/visits.json?exit_timestamp!=*&dwell>00:02:00&order=-id'.format(site)
		visitlist = run_command(command)
		print 'grabbing all open visits',len(visitlist)

		command = 'http://{0}/v2/bays.json'.format(site)
		baylist = run_command(command)
		print 'grabbing all bays',len(baylist)

		# grab all the images from visits
		failednum = 0
		for visit in visitlist[0:20]:
			currTime = datetime.today()
			date_time_format = "%Y-%m-%d_%H-%M-%S"
			timeString = currTime.strftime(date_time_format);
			
			visitid = str(visit['id'])
			bayid =   str(visit['bay_id'])
			if not visitid in visitidlist:
				for exposure in imageexposure:
					try:        
						bay_id = visit['bay_id']        
						#print bay_id
						bay = return_bay(bay_id)            
						#print bay
						view = bay['sensor']['extended_properties']['view']
						sensor = bay['sensor']['extended_properties']['ip_address']            
						imagename = '{0}_{1}_{2}_{3}.jpg'.format( sensor,view,exposure,timeString )
						polygon = bay['sensor']['polygon']
						point,size = rc.return_coords(polygon)
						x = point['x']
						y = point['y']
						sw = size['width']
						sh = size['height']
						print "imagename", imagename
						command = 'http://{0}/views/{1}.jpg?w=1600&h=1200&er={2}&q=80&x={3}&y={4}&sw={5}&sh={6}'.format(sensor,view,exposure,x,y,sw,sh)
						print command
						get_image(command,imagename)
					except:
						pass
		print visitidlist
		visitidlist.append(visitid)
		time.sleep(2)