# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 13:18:13 2016

@author: mark kudas

This script takes the output of lpr_score.py and calculates the optimum 
global exposure and optimum per sensor exposure.

"""

import pandas as pd
import argparse

if __name__ == "__main__":  
    parser = argparse.ArgumentParser(description=" this script grabs images from open visits to optimize exposure values")
    parser.add_argument('filepath', metavar='f',type=str,help='pathtofile')
    parser.add_argument("--verbose", help="see all calculations", action="store_true")

    args = parser.parse_args()
    filepath = args.filepath
    verbose = False
    if args.verbose:
        verbose = True

    # grab and process the data
    with open(filepath, 'r') as fh:
        data = [x.strip() for x in fh.readlines()]

    hours = []
    exposures = []
    sensors = [] 
    plates = []
    for line in data:
        sensors.append('-'.join(line.split('_')[0:2]))
        exposures.append(line.split('_')[2])
        hours.append(line.split('_')[4].split('-')[0])
        plates.append(line.split(',')[1])
        
    dfPlateResults = pd.DataFrame({'hour': hours,'sensorname':sensors,'exposure':exposures,'plate':plates})

    plated_images = dfPlateResults[dfPlateResults.plate != 'noplate']
    unplated_images = dfPlateResults[dfPlateResults.plate == 'noplate']

    # calculate best global exposure
    grouped_plates = plated_images.groupby('exposure')
    plates = grouped_plates.count().hour.tolist()
    
    grouped_totalentries = dfPlateResults.groupby('exposure')
    entries = grouped_totalentries.count().hour.tolist()

    imageexposure = ['0.2','0.4','0.6','0.8','1.0','1.2','1.4','1.6','1.8']
    accuracy = [x/float(y) for x,y in zip(plates,entries)]

    # calculate best exposure
    print "optimal exposure"
    for x in sorted([(x,y) for x,y in zip(accuracy,imageexposure)],reverse=True):
        print "exposure: ",x[1], "success ratio: ", x[0]

    print '--------------------'
    
    optimal_sensor_count = 0
    optimal_plate_count = 0
    optimal_maxplate_count = 0

    # calculate best exposure per sensor
    unique_sensors = list(set(sensors))
    for sens in unique_sensors:

        imageset = dfPlateResults[dfPlateResults.sensorname == sens]
        plated_images = imageset[imageset.plate != 'noplate']
        grouped_plates = plated_images.groupby('exposure')
        plates = grouped_plates.count().hour.to_dict()

        maxplates = 0
        for exp in ['0.2','1.0','1.8']:
            if exp in plates.keys():
                if plates[exp] > maxplates:
                    maxplates = plates[exp]

        grouped_totalentries = imageset.groupby('exposure')
        entries = grouped_totalentries.count().hour.to_dict()

        accuracy = {}
    
        for exp in imageexposure:
            if exp in plates.keys() and exp in entries.keys():
                accuracy[exp] = plates[exp] / float(entries[exp])
            else:
                accuracy[exp] = 0.0
     
        resultsdict  = {}
        accuracies = accuracy.values()
        for exp in imageexposure:
            acc = accuracy[exp]
            
            if str(acc) in resultsdict.keys():
                val = resultsdict[str(acc)]
                newval = val + [str(exp)]
                resultsdict[str(acc)] = newval
            else:
                resultsdict[str(acc)] = [str(exp)]

        best = False
        for k in iter(sorted(resultsdict.iteritems(),reverse=True)):
            if not best:
                best = k
            
        # calculate accuracy if best exposure per sensor is used
        # choose best exposure
        bestexposure = best[1][0]
        #print  "best exposure",bestexposure

        # add to accuracy
        if plates:
            optimal_maxplate_count += maxplates
            optimal_plate_count += plates[bestexposure]
            optimal_sensor_count += entries[bestexposure]
            #print optimal_plate_count , float(optimal_sensor_count)

    print "best accuracy {0}".format(optimal_plate_count / float(optimal_sensor_count))
    print "total plates {0}, total entries {1}".format(optimal_plate_count,optimal_sensor_count)
    print 
    print "0.2, 1.0, 1.8 tries accuracy {0}".format(optimal_maxplate_count / float(optimal_sensor_count))