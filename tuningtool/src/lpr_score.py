import requests
import os
import json
from datetime import datetime

#import pandas as pd
#import operator

'''
Script takes a directory of LPR images, runs them through the ARH engine, and creates a csv of the results

notes: this is the command for the ARH webservice
#command = 'http://localhost:8028/plates.json?file-path=c:/Users/Administrator/Desktop/lpr.jpg'

'''

def timestring():
    currTime = datetime.today()
    date_time_format = "%Y-%m-%d_%H-%M-%S"
    return currTime.strftime(date_time_format);

resultspath = '..\\results'
if not os.path.exists(resultspath):
    os.mkdir(resultspath)
results = '..\\results\\results_{0}.txt'.format(timestring())
imagepath = '..\\data\\'

def lpr_images(): 
    for root,directory,files in os.walk(imagepath):
        for f in files:
            filepath = os.path.join(root,f)
            print filepath
            command = 'http://localhost:8028/plates.json?file-path={0}&max-plate-searches=1'.format(os.path.abspath(filepath))
            r = requests.get(command,timeout=20)
            if r.status_code == 200:            
                text = json.loads(r.text)
                if text:                
                    platedict = text[0]
                    results_string = '{0},nokey'.format(f)
                    if 'Text' in text[0].keys():
                        results_string =  ','.join([f,platedict['Text']])
                else:
                    results_string = ','.join([f,'noplate'])
            with open(results,'a') as outfile:
                outfile.write(results_string + '\n')

if __name__ == "__main__":
    lpr_images()