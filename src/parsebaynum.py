# -*- coding: utf-8 -*-
"""
Created on Tue Mar 01 12:54:59 2016

@author: parkassist
"""

import sys

for line in sys.stdin:
    if 'Site' in line: 
        continue
    linelist =  line.split(',')
    site = linelist[0]
    if len(linelist) > 2:    
        baynumber = linelist[3].split('of')
        if len(baynumber) > 0:
            baynumber = baynumber[0]
        else:
            baynumber = "NaN"
    print "{0},{1}".format(site,baynumber)
