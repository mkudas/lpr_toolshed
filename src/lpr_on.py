import requests
import json

username = 'admin'
password = 'zxcvbnm'

def return_data(command):
	r = requests.get(command,auth=(username,password),timeout=20.0)
	if r.status_code == 200:
		#print r.text
		jsondata = json.loads(r.text)
	return jsondata
	
def visits(ip,name):
	total_visit = 1
	
	command = 'http://{0}/v2/status.json'.format(ip)
	log = return_data(command)
	#print log
	lprenabled = False

	if 'is_lpr_enabled' in log.keys():
		if log['is_lpr_enabled']:
			lprenabled = True
	print ','.join([name,str(lprenabled)])
	
			

if __name__ == "__main__":
	import sys

	for line in sys.stdin:
		name = line.strip().split(',')[0]
		ip =   line.strip().split(',')[1]
		try:
			vis = visits(ip,name)	
		except:
			print name,",fail"