import requests
import json

username = 'admin'
password = 'zxcvbnm'
namelist = [
'Omaha Airport',
'Stamford Transportation Center',
'Bradley Airport',
'Td Garden',
'Texas Medical Center',
'Montreal Airport',
'Cranbourne Park',
'Jersey Waterfront',
'East Village',
'Liberty Place',
'Tufts',
'Valley Fair',
'Celgene Hq',
'Mons',
'Village Mall',
'Multiplaza Escazu',
'Muscat',
'Multiplaza Panamericana',
'Humber River Hospital',
'King Faisal Specialist Hospital',
'Deira',
'Santa Monica Place',
'Lusail',
'Penrith',
'Garden State Plaza',
'Eastlands',
'Mirdif',
'Bahrain',
'Indooroopilly',
'Stratford City',
'Liverpool',
'Ft Lauderdale',
'Fashion Valley',
'Melbourne Airport',
'Miranda',
'Chadstone',
'Mall Of Emirates']

iplist = [
'192.168.237.128',
'192.168.237.109',
'192.168.237.127',
'192.168.237.119',
'192.168.237.131',
'192.168.237.129',
'192.168.237.84',
'192.168.237.169',
'192.168.237.77',
'192.168.237.107',
'192.168.237.124',
'192.168.237.120',
'192.168.237.125',
'192.168.237.171',
'192.168.237.203',
'192.168.237.204',
'192.168.237.167',
'192.168.237.206',
'192.168.237.121',
'192.168.237.151',
'192.168.237.162',
'192.168.237.104',
'192.168.237.164',
'192.168.237.78',
'192.168.237.100',
'192.168.237.89',
'192.168.237.163',
'192.168.237.161',
'192.168.237.87',
'192.168.237.152',
'192.168.237.81',
'192.168.237.114',
'192.168.237.122',
'192.168.237.46',
'192.168.237.76',
'192.168.237.56',
'192.168.237.166']

def return_data(command):
	r = requests.get(command,auth=(username,password),timeout=20.0)
	if r.status_code == 200:
		#print r.text
		jsondata = json.loads(r.text)
	return jsondata
	
def visits(ip,name):
	total_visit = 1
	
	command = 'http://{0}/v2/settings.json'.format(ip)
	log = return_data(command)
	ratios = ' '
	retries = ' '
	for item in log:
		#print item.keys()
		if 'component_name' in item.keys():
			if item['full_name'] == "EthernetCameraSensorPlugin.ExposureRatios":
				ratios = item['value'].replace(',','/')
	
			if item['full_name'] == "EthernetCameraSensorPlugin.MaxPlateRetries":
				retries = item['value']				
	print ','.join((name,retries,ratios))


if __name__ == "__main__":
	import sys

	for line in sys.stdin:
		name = line.strip().split(',')[0]
		ip =   line.strip().split(',')[1]
		try:
			vis = visits(ip,name)
		except:
			print "fail",name


'''
	sitelist = [{'sitename':x,'siteip':y} for x,y in zip(namelist,iplist)]
	results = []
	#try:
	for site in sitelist:
	#for site in test:
		#print site
		vis = visits(site)
	#except:
#		print "fail",site
			
	with open('lpr_setting_output.csv','w') as outfile:
		for x in results:
			outfile.write(x + '\n')
'''