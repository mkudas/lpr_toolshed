import requests
import json

username = 'admin'
password = 'zxcvbnm'

def return_data(command):
	r = requests.get(command,auth=(username,password),timeout=20.0)
	if r.status_code == 200:
		#print r.text
		jsondata = json.loads(r.text)
	return jsondata
	
def visits(ip,name):
	command = 'http://{0}/v2/settings.json'.format(ip)
	#print command
	log = return_data(command)
	#print log
	logging_enabled = False
	for item in log:
		#print item.keys()
		if 'name' in item.keys():
			if item['name'] == "EnableLprRequestTracking":
				logging_enabled = item['value'].replace(',','/')
	print ','.join([name,str(logging_enabled)])
	
			

if __name__ == "__main__":
	import sys

	for line in sys.stdin:
		name = line.strip().split(',')[0]
		ip =   line.strip().split(',')[1]
		try:
			vis = visits(ip,name)	
		except:
			print name,",fail"