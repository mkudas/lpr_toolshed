# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 13:18:13 2016

@author: parkassist
"""

import pandas as pd
filename = "C:\\Users\\parkassist\\Desktop\\LPR\\exposure\\data\\trial4_results\\trial4_results.txt"

with open(filename, 'r') as fh:
    data = [x.strip() for x in fh.readlines()]

print 'sensor,exposure,hour,plate'
hours = []
exposures = []
sensors = [] 
plates = []
for line in data:
    sensors.append('-'.join(line.split('_')[0:2]))
    exposures.append(line.split('_')[2])
    hours.append(line.split('_')[4].split('-')[0])
    plates.append(line.split(',')[1])
    
sm_ex = pd.DataFrame({'hour': hours,'sensorname':sensors,'exposure':exposures,'plate':plates})

unique_sensors = list(set(sensors))

plated_images = sm_ex[sm_ex.plate != 'noplate']
unplated_images = sm_ex[sm_ex.plate == 'noplate']

'''
for sens in unique_sensors:
    imageset = sm_ex[sm_ex.sensorname == sens]
    hours = list(set(imageset.hour))
    for hr in hours:
        exp = imageset[imageset.hour == hr]
        
        #e = list(set(exp.exposures))
'''     

'''
res = {}        
# exposure over bays, for hours
for hr in list(set(hours)):
    #print 'hour', hr
    hourfiltered = sm_ex[sm_ex.hour == hr]
    
    totalgroup = hourfiltered.groupby('exposure')
    totallist = totalgroup.count().hour.tolist()

    #print 'total',totalgroup.count()
    platesgroup = hourfiltered[hourfiltered.plate != 'noplate'].groupby('exposure')
    #print 'plates', platesgroup.count()
    plateslist = platesgroup.count().hour.tolist()
    success  = [a/float(b) for a,b in zip(plateslist,totallist)]
    print ','.join([hr,','.join([str(x) for x in success])])
'''     
# exposure per bay per hour
for sen in list(set(sensors))[0:10]:
    print "sensor", sen
    for hr in list(set(hours)):
        
        hourfiltered = sm_ex[(sm_ex.hour == hr) & (sm_ex.sensorname == sen)]
        
        totalgroup = hourfiltered.groupby('exposure')
        totallist = totalgroup.count().hour.tolist()
    
        #print 'total',totalgroup.count()
        platesgroup = hourfiltered[hourfiltered.plate != 'noplate'].groupby('exposure')
        #print 'plates', platesgroup.count()
        plateslist = platesgroup.count().hour.tolist()
        success  = [a/float(b) for a,b in zip(plateslist,totallist)]
        print ','.join([hr,','.join([str(x) for x in success])])

    
