/tuning tool

This is a tool to test if setting the exposure ratio on M3 sites makes a difference.

The first script, get_lpr_images.py will pull all the live visist that have a vehicle in them, take lpr images from a range of exposure ratios, and save them in ./data. The images will be encoded with the bay, visitid, timeofday and the exposure ratio.

usage: python get_lpr_images.py localhost <paseserverpassword>

The second script, lpr_score.py will take a directory of lpr images provided from the first script, post them to the ARH server and produce a file to be analyzed. This will be saved in ./results/results-<datestamp>.txt

usage: python lpr_score.py

The third script will take ./results/results-<datestamp>.txt and generate statistics. This can be run off site.

returncoords.py is a helper function to map out the exposure polygon for m3.

For these scripts to run, python must be installed on the site. The easiest way is to install is at http://conda.pydata.org/miniconda.html


-----------------
/hourly exposure



-----------------
/vicus calculator

This will dump a snapshot for use in calculating the vicus rate
usage: vicuscalculator.py sitename siteip pasepassword

This will produce a directory under ./data/vicus/<sitename>/ which will contain
a snapshot of images, also a .pkl of visits and bays.json and a .csv of the results in ./data/vicus/<sitename>.csv


/data

/anpr

/exposure

/src

/docs


