import math
def return_coords(polygon):
	print polygon
	ImageWidth = 1600
	ImageHeight = 1200
	MaxImageWidth = 2592
	MaxImageHeight = 1944
	xs = [polygon['x0'],polygon['x1'],polygon['x2'],polygon['x3']]
	ys = [polygon['y0'],polygon['y1'],polygon['y2'],polygon['y3']]

	topLeft = {'x':min(xs),'y':min(ys)}
	print "topleft", topLeft
	polygonBoundingBox = {'left':topLeft['x'], 'top':topLeft['y'], 'width':max(xs) - topLeft['x'], 'height':max(ys) - topLeft['y']}
	print "polygonBoundingBox", polygonBoundingBox

	polygonCenter = {'x':polygonBoundingBox['left'] + (polygonBoundingBox['width'] * 0.5),
	'y':polygonBoundingBox['top'] + (polygonBoundingBox['height'] * 0.5)}

	print "polygonCenter", polygonCenter
	
	if (polygonBoundingBox['width'] > polygonBoundingBox['height']):
		# Centers the imager area around the polygon center.
		# Polygon center is between 0 --> 1, so we scale it by the imager size.
		areaWidth = math.floor(MaxImageWidth * polygonBoundingBox['width'])
		print 'aWidtH',areaWidth
		# We just want to do optical zoom
		areaWidth = max([ImageWidth, areaWidth])
		print 'aWidtH',areaWidth
		#Looks weird but it's just to make sure we get a "stronger" number, dixit Ilan
		# "math.floor" is not right, for example math.floor(5) = 5 and math.floor(5 / 2) * 2 = 4
		areaWidth = math.floor(areaWidth / 4.0) * 4
		print 'aWidtH',areaWidth
		# var areaHeight = Math.math.floor(MaxImagerHeight * Math.Max(polygonBoundingBox.Width, polygonBoundingBox.Height))
		areaHeight = math.floor((areaWidth * 3) / 4)
		print 'aHeight',areaHeight
	else:
		# Centers the imager area around the polygon center.
		# Polygon center is between 0 --> 1, so we scale it by the imager size.
		areaHeight = math.floor(MaxImageHeight * polygonBoundingBox['height'])
		print 'aHeight',areaHeight

		# We just want to do optical zoom
		areaHeight = max([ImageHeight, areaHeight])
		print 'aHeight',areaHeight

		# Looks weird but it's just to make sure we get a "stronger" number, dixit Ilan # "math.floor" is not right, for example math.floor(5) = 5 and math.floor(5 / 2) * 2 = 4
		areaHeight = math.floor(areaHeight / 3.0) * 3.0
		print 'aHeight',areaHeight

		# var areaHeight = Math.math.floor(MaxImagerHeight * Math.Max(polygonBoundingBox.Width, polygonBoundingBox.Height))
		areaWidth = math.floor((areaHeight * 4) / 3.0)
		print 'aWidtH',areaWidth

	x = MaxImageWidth - ((polygonCenter['x'] * MaxImageWidth) + (areaWidth * 0.5))
	y = (polygonCenter['y'] * MaxImageHeight) - (areaHeight * 0.5)
	print 'x,y',x,y
	x = math.floor(min(MaxImageWidth - areaWidth - 1, max(0, x)));
	y = math.floor(min(MaxImageHeight - areaHeight - 1, max(0, y)));
	print 'x,y',x,y

	point = {'x':x,'y':y}
	size  = {'width':areaWidth,'height':areaHeight}
	return (point,size)

#poly =  { "x0": 0.206667, "x1": 0.443333, "x2": 0.485, "x3": 0.061666, "y0": 0.164444, "y1": 0.164444, "y2": 0.491111,"y3": 0.611111}
#print return_coords(poly)
#resolves to 
#http://10.174.192.102/views/1.jpg?w=1600&h=1200&q=80&er=1.3&sw=1600&sh=1200&x=991&y=153
