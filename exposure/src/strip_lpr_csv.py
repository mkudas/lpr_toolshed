import os

inputfile = 'C:\\Users\\parkassist\\Downloads\\documents-export-2016-03-14\\test.log.txt'
outputfile = 'C:\\Users\\parkassist\\Desktop\\LPR\\exposure\\results\\anr_zeros.csv'

with open(inputfile,'r') as fh:
	lines = [x.strip().split(',') for x in fh.readlines()]
	
baytable = {}
for i,line in enumerate(lines[2:]):
	#try:
	#print line[0].split('/')[4].split('_')
	print i
	bay = '_'.join(line[0].split('/')[4].split('_')[0:2])
	exposure = line[0].split('/')[4].split('_')[2]
	plate =  line[0].split('/')[4].split('_')[4].split('\t')[1]
	print bay,exposure,plate
	if bay in baytable.keys():
		baytable[bay][exposure] = plate
	else:
		baytable[bay] = {exposure:plate}
	#except:
	#	print "fail",line
	#	pass
print baytable['10.174.196.54_2']

def fmt(baytable,bay,exposure):

	if exposure in baytable[bay]:
		
		#return baytable[bay][exposure]
		if baytable[bay][exposure] == 'No Result':
			return '0'
		else:
			return '1'
	else:
		#return "none"
		return '0'

with open(outputfile,'w') as outfile:

	for bay in baytable.keys():
		line = ','.join([bay,fmt(baytable,bay,'0.4'),fmt(baytable,bay,'0.6'),fmt(baytable,bay,'0.8'),fmt(baytable,bay,'1.0'),fmt(baytable,bay,'1.2'),fmt(baytable,bay,'1.4'),fmt(baytable,bay,'1.6')]) + '\n'
		outfile.writelines(line)



