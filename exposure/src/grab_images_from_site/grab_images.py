import json
import requests
import os
from datetime import datetime
import time
# request for visist
#http://192.168.237.203/v2/visits.json?exit_timestamp!=*&dwell%3E00:02:00

# request for bays
#http://192.168.237.203/v2/bays.json


site = '192.168.237.104' # santa monica
site = '192.168.237.166' # moe
site = '192.168.10.4' # development

imagefolder = "C:\\Users\\parkassist\\Desktop\\LPR\\data\\exposure\\"
#imagefolder = "C:\\Users\\Administrator\\Desktop\\LPR\\data\\exposure\\"
if not os.path.exists(imagefolder):
    os.mkdir(imagefolder)

username = 'admin'
password = 'zxcvbnm'

visitidlist = []

imageexposure = ['0.2','0.4','0.6','0.8','1.0','1.2']

def run_command(command):
    r = requests.get(command,auth=(username,password),timeout=100.0)    
    print 'status', r.status_code
    text = 'none'
    if r.status_code == 200: 
        text = json.loads(r.text)
    return text
    
def get_image(command,imagename):
    print command
    r = requests.get(command,auth=(username,password),timeout=20.0)
    print r.status_code
    if r.status_code == 200:        
        filepath = os.path.join(imagefolder,imagename)
        print filepath
        with open(filepath, 'wb') as f:
            for chunk in r.iter_content():
                f.write(chunk)
    else:
        print "failed", imagename

def return_bay(bay_id):
    for bay in baylist:
        if bay['id'] == bay_id:
            return bay
    raise ValueError('Couldn\'t find bay')

while 1:
    # grab all open visits
    command = 'http://{0}/v2/visits.json?exit_timestamp!=*&dwell>00:02:00'.format(site)
    visitlist = run_command(command)
    print 'grabbing all open visits',len(visitlist)

    command = 'http://{0}/v2/bays.json'.format(site)
    baylist = run_command(command)
    print 'grabbing all bays',len(baylist)

    # grab all the images from visits
    failednum = 0
    for visit in visitlist:
        currTime = datetime.today()
        date_time_format = "%Y-%m-%d_%H-%M-%S"
        timeString = currTime.strftime(date_time_format);

        visitid = visit['id']
        print visitid
        if not visitid in visitidlist:
            for exposure in imageexposure:
                try:        
                    bay_id = visit['bay_id']        
                    print 'bay_id',bay_id
                    bay = return_bay(bay_id)            
                    print 'bay',bay
                    view = bay['sensor']['extended_properties']['view']
                    sensor = bay['sensor']['extended_properties']['ip_address']            
                    imagename = '{0}_{1}_{2}_{3}.jpg'.format( sensor,view,exposure,timeString )
                    print "imagename", imagename
                    command = 'http://{0}/views/{1}.ycc?w=1600&h=1200&e={2}'.format(sensor,view,exposure)
                    print command
                #get_image(command,imagename)
                except:
                    print "fail"
                    pass
            # add this 
        print visitidlist
        visitidlist.append(visitid)
        time.sleep(2)