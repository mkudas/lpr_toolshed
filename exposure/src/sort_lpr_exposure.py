import os
import shutil


inputfolder  = 'C:\Users\parkassist\Desktop\LPR\exposure\data'
outputfolder = 'C:\Users\parkassist\Desktop\LPR\exposure\output'

def extract_folder(imagename):
	print imagename
	outputfolder = 'C:\Users\parkassist\Desktop\LPR\exposure\output'
	foldername = imagename.split('_')[0:2]

	newfolder = '_'.join(foldername)
	print newfolder
	
	nnewfolder = os.path.join(outputfolder,newfolder)
	if not os.path.exists(nnewfolder):
		os.mkdir(nnewfolder)
	return nnewfolder

for root,folder,files in os.walk(inputfolder):
	for imagefile in files:
		
		imagepath = os.path.join(root,imagefile)
		
		outputfolder = extract_folder(imagefile)
		print "imagepath",imagepath
		print "outputfolder",outputfolder
		shutil.copy2(imagepath,outputfolder)
		#print outputfolder
