def return_coords(polygon):
'''	
		internal static Tuple<Point, Size> GetImagerArea(CameraSensor sensor)
		{
			var xs = new[] { sensor.Polygon.X0, sensor.Polygon.X1, sensor.Polygon.X2, sensor.Polygon.X3 };
			var ys = new[] { sensor.Polygon.Y0, sensor.Polygon.Y1, sensor.Polygon.Y2, sensor.Polygon.Y3 };

			var topLeft = new Point(xs.Min(), ys.Min());
			var polygonBoundingBox = new Rect(topLeft.X, topLeft.Y, xs.Max() - topLeft.X, ys.Max() - topLeft.Y);

			var polygonCenter = new Point(
				polygonBoundingBox.Left + (polygonBoundingBox.Width * 0.5),
				polygonBoundingBox.Top + (polygonBoundingBox.Height * 0.5));

			double areaWidth, areaHeight;

			if (polygonBoundingBox.Width > polygonBoundingBox.Height)
			{
				// Centers the imager area around the polygon center.
				// Polygon center is between 0 --> 1, so we scale it by the imager size.
				areaWidth = Math.Floor(MaxImageWidth * polygonBoundingBox.Width);

				// We just want to do optical zoom
				areaWidth = Math.Max(ImageWidth, areaWidth);

				// Looks weird but it's just to make sure we get a "stronger" number, dixit Ilan
				// "Floor" is not right, for example Floor(5) = 5 and Floor(5 / 2) * 2 = 4
				areaWidth = Math.Floor(areaWidth / 4.0) * 4;

				// var areaHeight = Math.Floor(MaxImagerHeight * Math.Max(polygonBoundingBox.Width, polygonBoundingBox.Height));
				areaHeight = Math.Floor((areaWidth * 3) / 4);
			}
			else
			{
				// Centers the imager area around the polygon center.
				// Polygon center is between 0 --> 1, so we scale it by the imager size.
				areaHeight = Math.Floor(MaxImageHeight * polygonBoundingBox.Height);

				// We just want to do optical zoom
				areaHeight = Math.Max(ImageHeight, areaHeight);

				// Looks weird but it's just to make sure we get a "stronger" number, dixit Ilan
				// "Floor" is not right, for example Floor(5) = 5 and Floor(5 / 2) * 2 = 4
				areaHeight = Math.Floor(areaHeight / 3.0) * 3;

				// var areaHeight = Math.Floor(MaxImagerHeight * Math.Max(polygonBoundingBox.Width, polygonBoundingBox.Height));
				areaWidth = Math.Floor((areaHeight * 4) / 3);
			}
			
			var x = MaxImageWidth - ((polygonCenter.X * MaxImageWidth) + (areaWidth * 0.5));
			var y = (polygonCenter.Y * MaxImageHeight) - (areaHeight * 0.5);

			// Obeys the imager area limits
			x = Math.Floor(Math.Min(MaxImageWidth - areaWidth - 1, Math.Max(0, x)));
			y = Math.Floor(Math.Min(MaxImageHeight - areaHeight - 1, Math.Max(0, y)));

			return Tuple.Create(new Point(x, y), new Size(areaWidth, areaHeight));
		}
'''