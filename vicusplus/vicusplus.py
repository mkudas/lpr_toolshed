import json

def printMatrix(m):
  print ' '
  for line in m:
	  spTupel = ()
	  breite = len(line)
	  for column in line:
		  spTupel = spTupel + (column, )
	  print "%3i"*breite % spTupel


def levenshtein(s1, s2):
	l1 = len(s1)
	l2 = len(s2)

	matrix = [range(l1 + 1)] * (l2 + 1)
	for zz in range(l2 + 1):
	  matrix[zz] = range(zz,zz + l1 + 1)
	for zz in range(0,l2):
		for sz in range(0,l1):
			if s1[sz] == s2[zz]:
		  		matrix[zz+1][sz+1] = min(matrix[zz+1][sz] + 1, matrix[zz][sz+1] + 1, matrix[zz][sz])
		else:
		  matrix[zz+1][sz+1] = min(matrix[zz+1][sz] + 1, matrix[zz][sz+1] + 1, matrix[zz][sz] + 1)
	#print "That's the Levenshtein-Matrix:"
	#printMatrix(matrix)
	return matrix[l2][l1]
  


resultsfile = 'sm.results.txt'
platefile = 'sm.plates.txt'
#http://192.168.237.114/v2/visits.json?entry_timestamp<2016-04-10&entry_timestamp>2016-04-11

with open(platefile,'r') as fh:
	platequeries = fh.read()

with open(resultsfile,'r') as fh:
	resultsreads = fh.read()
	results = json.loads(resultsreads)

platesonsite = []
for r in results:
	if 'plate' in r.keys():
		if len(r['plate']['text']) > 1:
			platesonsite.append(r['plate']['text'])


platereads = platequeries.split()
for plate in platereads:
	mindistance = 10
	minplate = ''
	for result in platesonsite:
		distance = levenshtein(result,plate)
		if distance < mindistance:
			mindistance = distance
			minplate = result
	if mindistance < 5:
		print "plate {0} best result {1} distance {2}".format(plate,minplate,mindistance)

